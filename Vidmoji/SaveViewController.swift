//
//  SaveViewController.swift
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 8/25/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import GUIPlayerView
import Photos
//import SCLAlertView
//import NVActivityIndicatorView
import AssetsLibrary
import Social
import FBSDKShareKit
import AVKit




class SaveViewController: UIViewController, GUIPlayerViewDelegate, NVActivityIndicatorViewable, UIDocumentInteractionControllerDelegate, AVPlayerViewControllerDelegate {
    // MARK: - Outlets

    @IBOutlet weak var stickersAndPlayerView: UIView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var sharingView: UIView!

    @IBOutlet weak var sharingHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var playerHalfBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerFullBottomConstraint: NSLayoutConstraint!
    // MARK: - Global Variables

    
    
    var playerViewController : AVPlayerViewController = AVPlayerViewController()

    
    
    var selectedAsset : PHAsset = PHAsset()
    var stickersArrays : Array<CLStickerView> = Array()
    var isVideoSaved : Bool = false
    var composition : AVMutableComposition = AVMutableComposition()
    var videoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
    var audioMix : AVMutableAudioMix = AVMutableAudioMix()
    var watermarkLayer : CALayer = CALayer()
    var selectedExporter : AVAssetExportSession!
    
    var savedURL : URL!
    var isComingFromInstagramShare : Bool = false
    var isComingFromFacebookShare : Bool = false

    // MARK: - ViewController Delegate Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        
        
        
        playerViewController.delegate = self
        
        
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: playerView.frame.size.width, height: playerView.frame.size.height)
        
        playerViewController.showsPlaybackControls = true
        // First create an AVPlayerItem
        
        let playerItem : AVPlayerItem = AVPlayerItem(url: selectedExporter.outputURL!)
        

        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        playerViewController.player = AVPlayer(playerItem: playerItem)


        playerViewController.player?.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
        
        
        playerView.addSubview(playerViewController.view)
        
        
        /*
        UIImage *normImage = [UIImage imageNamed:@"preview"];
        playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
        [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        [playBTN setImage:normImage forState:UIControlStateNormal];
        playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
        playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
        [_playerViewController.view addSubview:playBTN];
        */
        
        
        
        /*
        
        self.playerView.delegate = self
        guard selectedExporter != nil  else {
            return;
        }
        
        self.playerView.videoURL = selectedExporter.outputURL
        self.playerView.prepareAndPlayAutomatically(false)
        self.playerView.unHidePlayResizeButtons(true)
        */
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.stickersArrays = Array()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Button Actions
    @IBAction func saveAction(_ sender: AnyObject) {
        if isVideoSaved == true {
            SCLAlertView().showNotice("Already Saved.", subTitle: "The Video is already saved in the Photo Album.", closeButtonTitle: "OK")
        }
        else{
            let size = CGSize(width: 30, height:30)
            startAnimating(size, message: "Saving ...", type: NVActivityIndicatorType.ballClipRotatePulse)
            self.exportDidFinish(self.selectedExporter)
        }
    }
    @IBAction func backAction(_ sender: AnyObject) {
        //playerView.clean()
        //playerView.removeObserver(self, forKeyPath: "timedMetadata")
        self.performSegue(withIdentifier: "backToEmoji", sender: self)
    }
    @IBAction func instagramAction(_ sender: AnyObject) {
        if isComingFromInstagramShare == true {
            let captionString = "#videmoji"
            let encodedURL = self.savedURL.absoluteString.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)
            let encodedCaption = captionString.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)
            let urlString = String(format: "instagram://library?AssetPath=%@&InstagramCaption=%@", encodedURL!, encodedCaption!)
            let instagramURL = URL(string: urlString)
            if UIApplication.shared.canOpenURL(instagramURL!) {
                UIApplication.shared.openURL(instagramURL!)
            } else {
                print("Can't open Instagram")
                SCLAlertView().showNotice("Error", subTitle: "Please install Instagram application to share.", closeButtonTitle: "OK")
            }
        }
        else{
            self.saveAction(self)
            isComingFromInstagramShare = true
        }
    }
    @IBAction func facebookAction(_ sender: AnyObject) {
        
        
        if isComingFromFacebookShare == true {
            let sdkVideo = FBSDKShareVideo()
            sdkVideo.videoURL = self.savedURL
            let content = FBSDKShareVideoContent()
            content.video = sdkVideo
            FBSDKShareDialog.show(from: self, with: content, delegate: nil)
            
        }
        
        else{
            self.saveAction(self)
            isComingFromFacebookShare = true
        }
        
        
       
        
        
        
        
    }
    
    @IBAction func rateAction(_ sender: AnyObject) {
        //UIApplication.sharedApplication().openURL(NSURL(string : "itms-apps://itunes.apple.com/app/id1073026491")!)
        
        
        let appID = "1155495743" // Your AppID
        if let checkURL = URL(string: "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(appID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8") {
            if UIApplication.shared.openURL(checkURL) {
                print("url successfully opened")
            }
        } else {
            print("invalid url")
        }
    }
    
    
    // MARK: - Player delegate Methods
    
    func playerWillEnterFullscreen() {
        print("Entering Full screen")
        //self.sharingView.hidden = true
        self.playerHalfBottomConstraint.isActive = false
        self.view.layoutIfNeeded()
    }
    func playerWillLeaveFullscreen() {
        //self.sharingView.hidden = false
        
        guard self.playerHalfBottomConstraint != nil  else {
            return;
        }
        self.playerHalfBottomConstraint.isActive = true
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Video Export Method

    func exportDidFinish(_ session: AVAssetExportSession!) {
        if session.status == AVAssetExportSessionStatus.completed {
            let outputURL = session.outputURL
            let library = ALAssetsLibrary()
            
            if library.videoAtPathIs(compatibleWithSavedPhotosAlbum: outputURL) {
                
                
                
                
                library.writeVideoAtPath(toSavedPhotosAlbum: outputURL, completionBlock: { (assetURL, error) in
                    
                
                    
                    
                    
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.stopAnimating()
                    
                        
                        if (error != nil) {
                            //UIAlertView(title: "Error", message:  "Video Saving Failed", delegate: nil, cancelButtonTitle: "OK").show()
                            SCLAlertView().showError("Error", subTitle: "Video Saving Failed. Please Try Again.", closeButtonTitle: "OK")

                            
                            
                            
                        } else {
                            
                            self.isVideoSaved = true
                            
                            print("URL of Saved Video is \(assetURL?.absoluteString)")
                            self.savedURL = assetURL
                            
                            if self.isComingFromInstagramShare == true {
                                self.instagramAction(self)
                            }
                            else if self.isComingFromFacebookShare == true {
                                self.facebookAction(self)
                            }
                            else{
                                
                                if ALInterstitialAd.isReadyForDisplay() == true {
                                    ALInterstitialAd.show()
                                }
                                
                                
                                
                                SCLAlertView().showSuccess("Video Saved", subTitle: "Saved To Photo Album", closeButtonTitle: "OK")
                            }
                           
                            self.isComingFromInstagramShare = true
                            self.isComingFromFacebookShare = true
                            
                            
                            //UIAlertView(title: "Video Saved", message:  "Saved To Photo Album", delegate: nil, cancelButtonTitle: "OK").show()
                            //self.playerView.clean()

                            //self.performSegueWithIdentifier("SaveToVc", sender: self)
                        }
                    })
                })
            }
        }
    }
    
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
