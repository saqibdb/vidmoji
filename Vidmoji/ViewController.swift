//
//  ViewController.swift
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 8/16/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import Photos
//import SCLAlertView
//import NVActivityIndicatorView



@objc public protocol FSAlbumViewDelegate: class {
    
    func albumViewCameraRollUnauthorized()


}



var selectedVideoDict : NSDictionary = NSDictionary()
var selectedAsset : PHAsset = PHAsset()


var arrayOfVideos : [NSDictionary] = []




final class ViewController: UIViewController, PHPhotoLibraryChangeObserver, UIGestureRecognizerDelegate,NVActivityIndicatorViewable,UICollectionViewDelegate , UICollectionViewDataSource {
    

    @IBOutlet weak var galleryCollectioView: UICollectionView!
    
    var images: PHFetchResult<PHAsset>!
    var imageManager: PHCachingImageManager?
    var previousPreheatRect: CGRect = CGRect.zero
    var phAsset: PHAsset!
    weak var delegate: FSAlbumViewDelegate? = nil

    
    @IBOutlet weak var informationText: UILabel!
    
    
    
    
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.images == nil {
            return 0
        }
        return self.images.count
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
       
        
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCollectionViewCell", for: indexPath) as! galleryCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        // make cell more visible in our example project
        
        let currentTag = cell.tag + 1
        cell.tag = currentTag
        
        
        let cellSize = CGSize(width: (self.view.frame.width/3) - 20, height: (self.view.frame.width/3) - 20)

        let asset = self.images[(indexPath as NSIndexPath).item] 
        self.imageManager?.requestImage(for: asset,
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    
                                                    if cell.tag == currentTag {
                                                        cell.imageView.image = result
                                                    }
                                                    
        }
        
        
        return cell
        
        
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width/3) - 20, height: (self.view.frame.width/3) - 20)
    }
    
    internal func collectionView(_ collectionView: UICollectionView,
                               didSelectItemAt indexPath: IndexPath) {
        selectedAsset = self.images[(indexPath as NSIndexPath).row] 
        self.performSegue(withIdentifier: "galleryToEmoji", sender: self)
    
        
    
    }

    
    
    
    
    
    
    
    
    
    
    func initialize() {
        
        if images != nil {
            
            return
        }
        
       
    
        //galleryCollectioView.registerNib(UINib(nibName: "FSAlbumViewCell", bundle: NSBundle(forClass: self.classForCoder)), forCellWithReuseIdentifier: "FSAlbumViewCell")
        
        // Never load photos Unless the user allows to access to photo album
        checkPhotoAuth()
        
        // Sorting condition
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        
        images = PHAsset.fetchAssets(with: .video, options: options)
        
        if images.count > 0 {
            informationText.text = ""
            informationText.isHidden = true
            print("Images Gotten = \(self.images.count)")

            changeImage(images[0] )
            galleryCollectioView.reloadData()
            galleryCollectioView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
        }
        else {
            informationText.text = "You do not have any videos in the Gallery. This app requires videos present in gallery. Please Add Videos in Gallery to use it."
            informationText.isHidden = false
        }
        
        PHPhotoLibrary.shared().register(self)
        
    }
    
    //MARK: - PHPhotoLibraryChangeObserver
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        DispatchQueue.main.async {
            
            let collectionChanges = changeInstance.changeDetails(for: self.images as! PHFetchResult<PHObject>)
            if collectionChanges != nil {
                
                self.images = collectionChanges!.fetchResultAfterChanges as! PHFetchResult<PHAsset>
                
                print("Images Gotten = \(self.images.count)")
                
                let collectionView = self.galleryCollectioView!
                
                if !collectionChanges!.hasIncrementalChanges || collectionChanges!.hasMoves {
                    
                    collectionView.reloadData()
                    
                } else {
                    
                    collectionView.performBatchUpdates({
                        let removedIndexes = collectionChanges!.removedIndexes
                        if (removedIndexes?.count ?? 0) != 0 {
                            collectionView.deleteItems(at: removedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        let insertedIndexes = collectionChanges!.insertedIndexes
                        if (insertedIndexes?.count ?? 0) != 0 {
                            collectionView.insertItems(at: insertedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        let changedIndexes = collectionChanges!.changedIndexes
                        if (changedIndexes?.count ?? 0) != 0 {
                            collectionView.reloadItems(at: changedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        }, completion: nil)
                }
                
                self.resetCachedAssets()
            }
        }
    }
    
    func changeImage(_ asset: PHAsset) {
        
        self.phAsset = asset
        
        
        
        
        
        
        
        DispatchQueue.global().async(execute: {
            
            let options = PHImageRequestOptions()
            options.isNetworkAccessAllowed = true
            
            self.imageManager?.requestImage(for: asset,
                targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                contentMode: .aspectFill,
            options: options) {
                result, info in
                
                DispatchQueue.main.async(execute: {
                    
                   
                })
            }
        })
    }
    
    
    // Check the status of authorization for PHPhotoLibrary
    fileprivate func checkPhotoAuth() {
        
        PHPhotoLibrary.requestAuthorization { (status) -> Void in
            switch status {
            case .authorized:
                self.imageManager = PHCachingImageManager()
                if self.images != nil && self.images.count > 0 {
                    
                    self.changeImage(self.images[0] )
                }
                
            case .restricted, .denied:
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.delegate?.albumViewCameraRollUnauthorized()
                    SCLAlertView().showError("UnAthorized", subTitle: "App Requires Access to Gallery. Please go to settings and provide photo Access to the app.")
                    self.informationText.isHidden = false
                    self.informationText.text = "App Requires Access to Gallery. Please go to settings and provide photo Access to the app."
                    
                })
            default:
                break
            }
        }
    }
    // MARK: - Asset Caching
    
    func resetCachedAssets() {
        
        imageManager?.stopCachingImagesForAllAssets()
        previousPreheatRect = CGRect.zero
    }
    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        galleryCollectioView.delegate = self
        galleryCollectioView.dataSource = self
        
        galleryCollectioView.backgroundColor = UIColor.clear
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.initialize()
        //let size = CGSize(width: 30, height:30)
        //startActivityAnimating(size, message: "Fetching Videos ... ", type: NVActivityIndicatorType.BallClipRotatePulse)
        
        
        /*Utility().buildAssetsLibrary(nil) { (array : [AnyObject]!) in
            if array.count > 0 {
                arrayOfVideos = array as! [NSDictionary]
                self.galleryCollectioView.reloadData()
                self.stopActivityAnimating()
            }
        }
 */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("Memeory Issue ________________________")
    }
   
    @IBAction func unwindToVC(_ segue: UIStoryboardSegue) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "galleryToEmoji") {
            let newVc : EmojiViewController = segue.destination as! EmojiViewController
            newVc.selectedAsset = selectedAsset
        }
    }
    
}



internal extension IndexSet {
    
    func aapl_indexPathsFromIndexesWithSection(_ section: Int) -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        indexPaths.reserveCapacity(self.count)
        (self as NSIndexSet).enumerate({idx, stop in
            indexPaths.append(IndexPath(item: idx, section: section))
        })
        return indexPaths
    }
}



