//
//  UtilityObj.h
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 9/7/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilityObj : NSObject
+ (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate ;


+ (void)loadCameraRollAssetToInstagram:(NSURL*)assetsLibraryURL andMessage:(NSString*)message ;

@end
