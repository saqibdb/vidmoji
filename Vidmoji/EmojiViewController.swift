//
//  EmojiViewController.swift
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 8/17/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import ImojiSDKUI
import Masonry
import Photos
import GUIPlayerView.GUIPlayerView
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


//class EmojiViewController: UIViewController,NVActivityIndicatorViewable,IMToolbarDelegate,IMCollectionViewControllerDelegate , IMStickerSearchContainerViewDelegate, GUIPlayerViewDelegate, SQBEmojiDelegate, CLStickerToolDelegate {
class EmojiViewController: UIViewController,NVActivityIndicatorViewable,IMToolbarDelegate,IMCollectionViewControllerDelegate , IMStickerSearchContainerViewDelegate, CLStickerToolDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var videoView: GUIPlayerView!
    @IBOutlet weak var halfView: IMHalfScreenView!
    @IBOutlet weak var mainImageView: UIImageView!
    
   // @IBOutlet weak var emojiView: SQBEmojiView!
    
    // MARK: - Global Variables

    var selectedAsset : PHAsset = PHAsset()
    var imageManager: PHCachingImageManager?
    var tagsCount = 1
    var timer: Timer!
    var emojisArrays : Array<CLStickerView> = Array()
    var stickersArrays : Array<CLStickerView> = Array()


    var selectedStickerView : StickerView = StickerView()
    
    
    var exporter : AVAssetExportSession!
        
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    
    @objc func imageRecieved(notification: NSNotification){
        
        let size = CGSize(width: 30, height:30)
        
        startAnimating(size, message: "Extracting Sticker ...", type: NVActivityIndicatorType.ballClipRotatePulse)
        
        
        //do stuff
        let dict : NSDictionary = notification.userInfo! as NSDictionary
        let imageURL : URL = dict["imageURL"] as! URL
        //imageURL = imageURL.replacingOccurrences(of: "//", with: "///")
        //let escapedString = imageURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        //let url : URL = URL.init(fileURLWithPath: imageURL)
        print("Selected \(imageURL)")
        
        
        
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: imageURL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                
                if let imageData = data as Data? {
                    if self.videoView == nil {
                        return
                    }
                    
                    let serverImage : UIImage = UIImage(data: imageData)!
                    
                    let resizedImage : UIImage = self.resizeImage(image: serverImage, newWidth: 200.0)!
                    
                    let view : CLStickerView = CLStickerView.init(image: resizedImage)
                
                    view.center = CGPoint.init(x: self.videoView.width/2, y: self.videoView.height/2)
                        
                    view.delegate = self
                    let ratio : CGFloat = min( (0.3 * self.view.width) / view.width, (0.3 * self.view.height) / view.height);

                   view.setScale(ratio)
                    self.videoView.addSubview(view)

                    CLStickerView.setActive(view)
                    
                    if self.emojisArrays.contains(view) == false {
                        self.emojisArrays.append(view)
                    }
                    
                    /*
                    let stickerView : StickerView = StickerView( frame: CGRect(x: 50, y: 50, width: 110, height: 110), andImage: imageData)
                    self.videoView.addSubview(stickerView)
                    let pan = UIPanGestureRecognizer(target:self, action:#selector(EmojiViewController.pan(_:)))
                    stickerView.addGestureRecognizer(pan)
                    let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.scaleWithPinch(_:)))
                    stickerView.addGestureRecognizer(pinch)
                    stickerView.isMultipleTouchEnabled = true
                    stickerView.resizeBtn.addTarget(self, action: #selector(self.addPanGesture), for: UIControlEvents.touchUpInside)
                    stickerView.deleteBtn.setBackgroundImage(UIImage(named: "closeIcon"), for: UIControlState())
                    stickerView.resizeBtn.setBackgroundImage(UIImage(named: "resizeIcon"), for: UIControlState())
                    stickerView.tickBtn.setBackgroundImage(UIImage(named: "tickIcon"), for: UIControlState())
                    stickerView.rotateBtn.setBackgroundImage(UIImage(named: "rotateIcon"), for: UIControlState())
                    stickerView.rotateBtn.addTarget(self, action: #selector(self.addPanGesture), for: UIControlEvents.touchUpInside)
                    
                    
                    stickerView.resizeBtn.tag = self.tagsCount
                    self.tagsCount = self.tagsCount + 1
                    stickerView.resizeBtn.addTarget(self, action: #selector(EmojiViewController.wasDragged(_:event:)), for: UIControlEvents.touchDragInside)
                    stickerView.rotateBtn.addTarget(self, action: #selector(EmojiViewController.wasDraggedForRotation(_:event:)), for: UIControlEvents.touchDragInside)
                    //stickerView.rotateBtn.addTarget(self, action: #selector(EmojiViewController.buttonUp(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside])
                    stickerView.deleteBtn.addTarget(self, action: #selector(self.deleteStickerView), for: .touchUpInside)
                    stickerView.tickBtn.addTarget(self, action: #selector(self.tickStickerView), for: .touchUpInside)
                    
                    self.selectedStickerView = stickerView
                    */
                    
                    
                }
                else{
                    SCLAlertView().showError("Error", subTitle: "Error Extracting Sticker. Please Try Again!")
                    
                    //show error
                }
                self.stopAnimating()
                
                
            }
        }
        
    }
    
    func clStickerToolDidClose(_ sticker: CLStickerView!) {
        if self.emojisArrays.contains(sticker) {
           self.emojisArrays.remove(at: self.emojisArrays.index(of: sticker)!)
         }
    }
        
    // MARK: - ViewController Delegate Methods
    func handleTapForvideoView(_ sender: UITapGestureRecognizer) {
        let tempStickerView : CLStickerView = CLStickerView(image: nil)
        CLStickerView.setActive(tempStickerView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.imageRecieved(notification:)), name: Notification.Name("MessageREcieved"), object: nil)

        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapForvideoView(_:)))
        
        self.videoView.addGestureRecognizer(tap)
        
        self.videoView.isUserInteractionEnabled = true
        
        
        let cellSize = CGSize(width: mainImageView.frame.width, height: mainImageView.frame.height)
        self.imageManager = PHCachingImageManager()
        self.imageManager?.requestImage(for: selectedAsset,
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    self.mainImageView.image = result
        }
        getAssetUrl(self.selectedAsset) { (responseURL) in
            if let _ = responseURL {
                
                print("Selected Videos url \(responseURL)")
                DispatchQueue.main.async(execute: {
                    self.view.layoutIfNeeded()
                    //self.videoView.delegate = self
                    self.videoView.videoURL = responseURL
                    self.videoView.prepareAndPlayAutomatically(false)
                    self.videoView.unHidePlayResizeButtons(false)
                })
            }
        }
        // Do any additional setup after loading the view.
        self.view.layoutIfNeeded()
        
        
        makeHTTPGetRequest()
        
        
        

    }
    func makeHTTPGetRequest() {
        
        
        let url = URL(string: "http://ibuildx.com/dev/ServerCheckVidEmoji.php")
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            
            DispatchQueue.main.async {
                if(error != nil){
                    print("error")
                    //self.emojiView.setupSQBEmojis()
                }else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        
                        print("Entered the completionHandler\(json)")
                        
                        let status : String = json["status"] as! String
                        
                        if status == "enable" {
                            print("YES")
                            //self.emojiView.setupSQBEmojis()
                            self.setupImojiView()

                        }
                        else{
                            print("NO")
                            self.setupImojiView()
                        }
                        
                        
                    }catch let error as NSError{
                        print(error)
                        //self.emojiView.setupSQBEmojis()
                        self.setupImojiView()

                    }

                }
            }
            
            
            
                
        }).resume()
        
    
    }
    
    

    /*override func viewWillAppear(animated: Bool) {
        for var stickersView : StickerView in self.stickersArrays {
            stickersView.frame = stickersView.stickerFrame
            self.videoView.addSubview(stickersView)
            print(stickersView.frame)
        }
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        /*for stickersView : CLStickerView in self.stickersArrays {
            stickersView.frame = stickersView.stickerFrame
            guard self.videoView != nil  else {
                return;
            }
                self.videoView.addSubview(stickersView)
            
            print("Sticker Frame = \(stickersView.frame)")
            
        }
        */
    }
    
    func sqbEmojiDidExportImage(forSticker imageURL: String!) {
    
        
        
    }
    
    // MARK: - Button Actions

    @IBAction func backAction(_ sender: AnyObject) {
        self.videoView.clean()
        self.performSegue(withIdentifier: "backToVc", sender: self)
    }

    @IBAction func previewAction(_ sender: AnyObject) {
        if videoView.isPlaying() {
            videoView.pause()
        }
        else{
            videoView.play()
        }
    }
    @IBAction func exportAction(_ sender: AnyObject) {
        
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: "Encoding ...", type: NVActivityIndicatorType.ballClipRotatePulse)
        
       getAssetUrl(self.selectedAsset) { (responseURL) in
            if let _ = responseURL {
                let vidAsset = AVURLAsset(url: responseURL!, options: nil)
                
                self.videoOutput(vidAsset)
                
            }
            else{
                print("Issie Here")
        }
            
            //self.videoView.clean()

        }
        /*let size = CGSize(width: 30, height:30)
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType.BallClipRotatePulse)
        performSelector(#selector(delayedStopActivity),withObject: nil,afterDelay: 2.5)*/
    }
    func deleteStickerView (_ buttn : UIButton )
    {
        let stickerView = buttn.superview as! StickerView
        stickerView.removeFromSuperview()
        //if self.emojisArrays.contains(stickerView) {
         //   self.emojisArrays.remove(at: self.emojisArrays.index(of: stickerView)!)
       // }
        
        
    }
    func tickStickerView (_ buttn : UIButton )
    {
        let stickerView = buttn.superview as! StickerView
        stickerView.stickerImageViewOuter.layer.borderWidth = 0.0
        stickerView.gestureRecognizers?.removeAll(keepingCapacity: false)
        stickerView.deleteBtn.isHidden = true
        stickerView.tickBtn.isHidden = true
        stickerView.rotateBtn.isHidden = true
        stickerView.resizeBtn.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(EmojiViewController.refreshForEdit(_:)))
        stickerView.addGestureRecognizer(tap)
        
        
        //if self.emojisArrays.contains(stickerView) == false {
       //     self.emojisArrays.append(stickerView)
       // }

    }
    // MARK: - Gesture Actions

    func wasDragged (_ buttn : UIButton, event :UIEvent)
    {
        let touch : UITouch = (event.touches(for: buttn)?.first)! as UITouch
        let previousLocation : CGPoint = touch .previousLocation(in: buttn)
        let location : CGPoint = touch .location(in: buttn)
        let delta_x :CGFloat = location.x - previousLocation.x
        let delta_y :CGFloat = location.y - previousLocation.y
        buttn.center = CGPoint(x: buttn.center.x + delta_x,
                                   y: buttn.center.y + delta_y);
        let stickerView = buttn.superview! as! StickerView
        stickerView.gestureRecognizers?.removeAll(keepingCapacity: false)
        stickerView.setFrames(buttn.frame)
    }
    
    
    func wasDraggedForRotation (_ buttn : UIButton, event :UIEvent)
    {
        
        
        
        /*let touch : UITouch = (event.touchesForView(buttn)?.first)! as UITouch
        let previousLocation : CGPoint = touch .previousLocationInView(buttn)
        let location : CGPoint = touch .locationInView(buttn)
        let delta_x :CGFloat = location.x - previousLocation.x
        let delta_y :CGFloat = location.y - previousLocation.y
        buttn.center = CGPointMake(buttn.center.x + delta_x,
                                   buttn.center.y + delta_y);
        
 */
        
        let touch : UITouch = (event.touches(for: buttn)?.first)! as UITouch
        let previousLocation : CGPoint = touch .previousLocation(in: buttn)
        let location : CGPoint = touch .location(in: buttn)
        let delta_x :CGFloat = location.x - previousLocation.x
        let delta_y :CGFloat = location.y - previousLocation.y
        //buttn.center = CGPointMake(buttn.center.x + delta_x, buttn.center.y + delta_y);
        let stickerView = buttn.superview as! StickerView
        
        var rotateValue : CGFloat = 0.0
        
        if delta_y > 0 {
            
            rotateValue = delta_y * CGFloat(M_PI/180)
            
        }
        else if delta_y < 0 {
            rotateValue = delta_y * CGFloat(M_PI/180)
        }
        else {
            rotateValue = delta_x * CGFloat(M_PI/180)

        }
        let transform : CGAffineTransform = stickerView.transform.rotated(by: (rotateValue))
        stickerView.transform = transform;
        stickerView.gestureRecognizers?.removeAll(keepingCapacity: false)

        
        
        let RdnVal = CGFloat(atan2f(Float(stickerView.transform.b), Float(stickerView.transform.a)))
        let DgrVal = RdnVal * CGFloat(180 / M_PI)
        
        
        print("\(DgrVal)")
    }
    
    
    func rapidRotate() {
        UIView.animate(withDuration: 0.25, animations:{
            let degrees : CGFloat = 10 //the value in degrees
            let transform : CGAffineTransform = self.selectedStickerView.stickerImageView.transform.rotated(by: (degrees * CGFloat(M_PI/180)))
            self.selectedStickerView.stickerImageView.transform = transform;
        })
    }
    
    
    func startRotation() {
        
    }
    func refreshForEdit(_ sender: UITapGestureRecognizer? = nil) {
        let stickerView = sender?.view as! StickerView
        stickerView.stickerImageViewOuter.layer.borderWidth = 2.0
        stickerView.deleteBtn.isHidden = false
        stickerView.tickBtn.isHidden = false
        stickerView.rotateBtn.isHidden = false
        stickerView.resizeBtn.isHidden = false
        let pan = UIPanGestureRecognizer(target:self, action:#selector(EmojiViewController.pan(_:)))
        stickerView.addGestureRecognizer(pan)
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.scaleWithPinch(_:)))
        stickerView.addGestureRecognizer(pinch)
        stickerView.isMultipleTouchEnabled = true

    }
    
    
    func addPanGesture (_ buttn : UIButton )
    {
        let stickerView = buttn.superview as! StickerView
        let pan = UIPanGestureRecognizer(target:self, action:#selector(EmojiViewController.pan(_:)))
        stickerView.addGestureRecognizer(pan)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.scaleWithPinch(_:)))
        stickerView.addGestureRecognizer(pinch)
    }
    func pan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.videoView)
        sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.videoView)
        if let _ = timer {
            timer.invalidate()
        }
    }
    
    func scaleWithPinch(_ sender: UIPinchGestureRecognizer) {
        let stickerView = sender.view as! StickerView

        stickerView.transform = stickerView.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        
        setSizeForView(stickerView.tickBtn)
        setSizeForView(stickerView.deleteBtn)
        setSizeForView(stickerView.rotateBtn)
        setSizeForView(stickerView.resizeBtn)


    }
    func buttonUp(_ sender: AnyObject) {
        timer.invalidate()
    }

    // MARK: - Imoji Delegate

    func userDidSelectImoji(_ imoji: IMImojiObject, from collectionView: IMCollectionView) {
        let size = CGSize(width: 30, height:30)
        
        startAnimating(size, message: "Extracting Sticker ...", type: NVActivityIndicatorType.ballClipRotatePulse)
        
    
        
        
        
        let renderOptions : IMImojiObjectRenderingOptions = IMImojiObjectRenderingOptions.init(renderSize: .sizeThumbnail, borderStyle: .sticker, imageFormat: .PNG)
        
        
        let url : URL = imoji.getUrlFor(renderOptions)!
        print("Selected \(url)")
        
        
        
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {

                if let imageData = data as Data? {
                    let stickerView : StickerView = StickerView( frame: CGRect(x: 50, y: 50, width: 110, height: 110), andImage: imageData)
                    self.videoView.addSubview(stickerView)
                    let pan = UIPanGestureRecognizer(target:self, action:#selector(EmojiViewController.pan(_:)))
                    stickerView.addGestureRecognizer(pan)
                    let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.scaleWithPinch(_:)))
                    stickerView.addGestureRecognizer(pinch)
                    stickerView.isMultipleTouchEnabled = true
                    stickerView.resizeBtn.addTarget(self, action: #selector(self.addPanGesture), for: UIControlEvents.touchUpInside)
                    stickerView.deleteBtn.setBackgroundImage(UIImage(named: "closeIcon"), for: UIControlState())
                    stickerView.resizeBtn.setBackgroundImage(UIImage(named: "resizeIcon"), for: UIControlState())
                    stickerView.tickBtn.setBackgroundImage(UIImage(named: "tickIcon"), for: UIControlState())
                    stickerView.rotateBtn.setBackgroundImage(UIImage(named: "rotateIcon"), for: UIControlState())
                    stickerView.rotateBtn.addTarget(self, action: #selector(self.addPanGesture), for: UIControlEvents.touchUpInside)
                    
                    
                    stickerView.resizeBtn.tag = self.tagsCount
                    self.tagsCount = self.tagsCount + 1
                    stickerView.resizeBtn.addTarget(self, action: #selector(EmojiViewController.wasDragged(_:event:)), for: UIControlEvents.touchDragInside)
                    stickerView.rotateBtn.addTarget(self, action: #selector(EmojiViewController.wasDraggedForRotation(_:event:)), for: UIControlEvents.touchDragInside)
                    //stickerView.rotateBtn.addTarget(self, action: #selector(EmojiViewController.buttonUp(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside])
                    stickerView.deleteBtn.addTarget(self, action: #selector(self.deleteStickerView), for: .touchUpInside)
                    stickerView.tickBtn.addTarget(self, action: #selector(self.tickStickerView), for: .touchUpInside)
                    
                    self.selectedStickerView = stickerView
                    
                    
                    
                }
                else{
                    SCLAlertView().showError("Error", subTitle: "Error Extracting Sticker. Please Try Again!")
                    
                    //show error
                }
                self.stopAnimating()
            
            
            }
        }
  
    }
    

    
    // MARK: - Utility
    
    internal func getAssetUrl(_ mPhasset : PHAsset, completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        
        if mPhasset.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            mPhasset.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable: Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL)
            })
        } else if mPhasset.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: mPhasset, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable: Any]?) -> Void in
                
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl : URL = urlAsset.url
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
        
    }
    func setupImojiView() {
        let  session : IMImojiSession = IMImojiSession();
        let halfScreenView : IMHalfScreenView = IMHalfScreenView(session: session)
        halfScreenView.frame = CGRect(x: 0, y: 0, width: halfView.frame.size.width , height: halfView.frame.size.height )
        halfScreenView.delegate = self
        halfView.addSubview(halfScreenView)
        
        let options : IMCategoryFetchOptions = IMCategoryFetchOptions.init(classification: IMImojiSessionCategoryClassification.trending)
        halfScreenView.imojiSuggestionView.collectionView.loadImojiCategories(with: options)
    }
    
    func delayedStopActivity() {
        self.stopAnimating()
        SCLAlertView().showNotice("Under Construction", subTitle: "This feature is under Contstruction at the moment.")
    }
    
    func setSizeForView(_ view : UIView)  {
        var newFrame : CGRect = view.frame;
        newFrame.size = CGSize(width: 20.0, height: 20.0);
        view.frame = newFrame;
    }
    
    // MARK: - Navigation
     @IBAction func unwindToEmoji(_ segue: UIStoryboardSegue) {
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "EmojiToSave") {
            let newVc : SaveViewController = segue.destination as! SaveViewController
            newVc.selectedAsset = selectedAsset
            
            
            newVc.stickersArrays = self.stickersArrays
            
            newVc.selectedExporter = self.exporter
        }
    }
    
    
    // MARK: - Emoji to Vdeo Functions

    func videoOutput(_ videoAsset: AVAsset) {
        // 1 - Early exit if there's no video file selected
        
        self.stickersArrays.removeAll()
        
        for var stickersView in self.emojisArrays {
            stickersView.stickerFrame = stickersView.frame
            self.stickersArrays.append(stickersView)
            print("Sticker Frame = \(stickersView.frame)")

        }
        
        
        
        // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        
        // 3 - Video track
        let videoTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        
        let clipAudioTrack : AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeAudio)[0]
        
        
        let compositionAudioTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))

        


        
        do {
            try videoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0] , at: kCMTimeZero)
            try compositionAudioTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: clipAudioTrack , at: kCMTimeZero)

        } catch _ {
            
            print("Some exception caught")
        }
        
        // 3.1 - Create AVMutableVideoCompositionInstruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
        
        
        // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
        let videoLayerIntruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        
        let videoAssetTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0]
        
        var videoAssetOrientation_: UIImageOrientation = .up
        var isVideoAssetPortrait_: Bool = false
        
        let videoTransform:CGAffineTransform = videoAssetTrack.preferredTransform
        
        if videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0 {
            videoAssetOrientation_ = .right
            isVideoAssetPortrait_ = true
        }
        if videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0 {
            videoAssetOrientation_ = .left
            isVideoAssetPortrait_ = true
        }
        if videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0 {
            videoAssetOrientation_ = .up
        }
        if videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0 {
            videoAssetOrientation_ = .down
        }
        
        videoLayerIntruction.setTransform(videoAssetTrack.preferredTransform, at: kCMTimeZero)
        videoLayerIntruction.setOpacity(0.0, at: videoAsset.duration)
        
        
        // 3.3 - Add instructions
        mainInstruction.layerInstructions = [videoLayerIntruction]
        
        let mainCompositionInst = AVMutableVideoComposition()
        
        var naturalSize = CGSize()
        
        if isVideoAssetPortrait_ {
            naturalSize = CGSize(width: videoAssetTrack.naturalSize.height, height: videoAssetTrack.naturalSize.width)
        } else {
            naturalSize = videoAssetTrack.naturalSize
        }
        
        var renderWidth, renderHeight: CGFloat
        
        renderWidth = naturalSize.width
        renderHeight = naturalSize.height
        
        mainCompositionInst.renderSize = CGSize(width: renderWidth, height: renderHeight)
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTimeMake(1, 30)
        
        print("natural size = \(naturalSize)")
        print("Player size = \(self.videoView.frame.size)")
        
        var multiplier : CGFloat = 0.0
        
        var isWidthVideo : Bool = false
        
        if naturalSize.width > naturalSize.height {
            
            multiplier = naturalSize.width / self.videoView.frame.size.width
            isWidthVideo = true
            
        }
        else{
            multiplier = naturalSize.height / self.videoView.frame.size.height
            isWidthVideo = false
        }
        
        self.applyVideoEffectsToComposition(mainCompositionInst, size: naturalSize , multipleir : multiplier ,widthVideo : isWidthVideo)
        
        
        // 4 - Get path
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        
        let documentsDirectory = paths[0] as! NSString
        let date = NSDate()
        let timestamp = UInt64(floor(date.timeIntervalSince1970 * 1000))
        let myPathDocs = documentsDirectory.appendingPathComponent(String().appendingFormat("VidEmojiVideo-%d.mov", timestamp))
        let url = URL(fileURLWithPath: myPathDocs)
        
        
        print("path url is \(url)")
        // 5 - Create exporter
        exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL = url
        exporter.outputFileType = AVFileTypeQuickTimeMovie
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.videoComposition = mainCompositionInst
        
        exporter.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async(execute: {
                //self.exportDidFinish(exporter)
                
                
                
                
                self.stopAnimating()
                
                
                self.performSegue(withIdentifier: "EmojiToSave", sender: self)

                
            })
        })
        
    }
    func applyVideoEffectsToComposition(_ composition: AVMutableVideoComposition!, size: CGSize, multipleir : CGFloat, widthVideo : Bool) {
        
        // 1 - Set up the text layer
        /*let subTitle1Text = CATextLayer()
         subTitle1Text.font = "Helvetica-Bold"
         subTitle1Text.frame = CGRectMake(0, 0, size.width, 100)
         subTitle1Text.string = self.subTitle1.text
         subTitle1Text.alignmentMode = kCAAlignmentCenter
         subTitle1Text.foregroundColor = UIColor.whiteColor().CGColor*/
        
        
        // 2 - The usual overlay
        let overlayLayer = CALayer()
        
        
        
        
        
        for stickersView : CLStickerView in self.stickersArrays {
            
            
            
            let myImage : UIImage = stickersView.imageView().image!
            let aLayer : CALayer = CALayer()
            aLayer.contents = myImage.cgImage
            aLayer.contentsGravity = kCAGravityResizeAspect
            //aLayer.backgroundColor = UIColor.blue.cgColor

            
            
            let RdnVal = CGFloat(atan2f(Float(stickersView.transform.b), Float(stickersView.transform.a)))
            let DgrVal : CGFloat = RdnVal * CGFloat(180 / M_PI)

            let radians = CGFloat((DgrVal * CGFloat(M_PI / 180)) * -1)

            
            aLayer.transform = CATransform3DMakeRotation(radians, 0.0, 0.0, 1.0)
            //aLayer.transform = CGAffineTransform(rotationAngle: radians)


        
            let newMultiplier = multipleir - (multipleir / 5)
            
            if UIScreen.main.isRetina() {
                //newMultiplier = newMultiplier / 2
            }

            
            let width : CGFloat = (stickersView.stickerFrame.size.width) * newMultiplier
            let height : CGFloat = (stickersView.stickerFrame.size.height) * newMultiplier
            
            if widthVideo == true {
                let xAxis : CGFloat = (stickersView.stickerFrame.origin.x + 10) * multipleir
                let yAxis : CGFloat = size.height - (((stickersView.stickerFrame.origin.y + 10) * multipleir) + height)
                aLayer.frame = CGRect(x: xAxis, y: yAxis , width: width, height: height)

            }
            else {
                
                let midPointCanvas : CGFloat = self.videoView.frame.size.width / 2
                let midPointVideo : CGFloat = size.width / 2
                let midPointCanvasVideo : CGFloat = (size.width / multipleir) / 2

                print("midPointCanvas = \(midPointCanvas)")
                print("midPointVideo = \(midPointVideo)")
                print("midPointCanvasVideo = \(midPointCanvasVideo)")
                
                
                let midPointCanvasDifference : CGFloat = (stickersView.stickerFrame.origin.x + 10) - midPointCanvas
                let stickerFrameXwithVdeo : CGFloat = (midPointCanvasVideo + midPointCanvasDifference ) * multipleir
                
                let xAxis : CGFloat = stickerFrameXwithVdeo
                let yAxis : CGFloat = size.height - (((stickersView.stickerFrame.origin.y + 10) * newMultiplier) + height)
                
                
                
               //let yAxis : CGFloat = (stickersView.stickerFrame.origin.y) * newMultiplier
                //let xAxis : CGFloat = size.width - ((stickersView.stickerFrame.origin.x * newMultiplier) + width)
                aLayer.frame = CGRect(x: xAxis, y: yAxis , width: width, height: height)

                
                
                
            }
            
            
            
            

            
            
            
            //aLayer.frame = CGRectMake(((size.width) / multipleir - stickersView.stickerFrame.origin.x), ((size.height) / multipleir - stickersView.stickerFrame.origin.y) , (stickersView.stickerFrame.size.width) * multipleir, (stickersView.stickerFrame.size.height) * multipleir)
            
            //Needed for proper display. We are using the app icon (57x57). If you use 0,0 you will not see it
            aLayer.opacity = 1.0;
            
            overlayLayer.addSublayer(aLayer)
            
            print("putted fram is = \(aLayer.frame)")
            
            
        }
        
        
        
        
        overlayLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        overlayLayer.masksToBounds = true
        
        
        // 3 - set up the parent layer
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(overlayLayer)
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    
    
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIScreen {
    public func isRetina() -> Bool {
        return screenScale() >= 2.0
    }
    
    public func isRetinaHD() -> Bool {
        return screenScale() >= 3.0
    }
    
    fileprivate func screenScale() -> CGFloat? {
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            return UIScreen.main.scale
        }
        return nil
    }
}
