//
//  StickerView.swift
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 8/24/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class StickerView: UIView {
    
    var stickerFrame : CGRect = CGRect()
    
    
    
    //let stickerOuterView : UIView = UIView()

    
    
    let stickerImageView : UIImageView = UIImageView()
    let stickerImageViewOuter : UIView = UIView()

    let resizeBtn : UIButton = UIButton()
    let deleteBtn : UIButton = UIButton()
    let tickBtn : UIButton = UIButton()
    let rotateBtn : UIButton = UIButton()

    convenience init(frame: CGRect ,andImage imageData : Data) {
        self.init(frame : frame)
        //self.stickerOuterView.frame = frame
        
        
        
        
        self.stickerImageViewOuter.frame = CGRect(x: 10, y: 10, width: frame.size.width - 20, height: frame.size.height - 20)
        
        self.stickerImageView.frame = CGRect(x: 0, y: 0, width: frame.size.width - 20, height: frame.size.height - 20)
        self.stickerImageView.image = UIImage(data: imageData)
        self.stickerImageView.contentMode = .scaleAspectFit
    
        self.stickerImageViewOuter.layer.borderWidth = 2
        self.stickerImageViewOuter.layer.borderColor = UIColor(red:253/255.0, green:128/255.0, blue:44/255.0, alpha: 1.0).cgColor
        
        
        
        
        self.stickerImageView.clipsToBounds = false
        self.rotateBtn.frame = CGRect(x: frame.size.width - 20, y: frame.size.height - 20, width: 20, height: 20)
        self.deleteBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        self.tickBtn.frame = CGRect(x: frame.size.width - 20, y: 0, width: 20, height: 20)
        //self.rotateBtn.frame = CGRectMake(0, frame.size.height - 20, 20, 20)
        stickerImageViewOuter.addSubview(self.stickerImageView)
        
        //self.stickerOuterView.userInteractionEnabled = true
        self.isUserInteractionEnabled = true

        //self.addSubview(self.stickerOuterView)
        
        self.addSubview(self.stickerImageViewOuter)
        //self.addSubview(self.resizeBtn)
        self.addSubview(self.deleteBtn)
        self.addSubview(self.tickBtn)
        self.addSubview(self.rotateBtn)
        
        //self.stickerImageViewOuter.backgroundColor = UIColor.orangeColor()

    }
    
    func setFrames(_ buttonframe : CGRect) {
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: buttonframe.origin.x + 20, height: buttonframe.origin.y + 20)
        
        
        self.stickerImageViewOuter.frame = CGRect( x: 10,  y: 10, width: self.frame.width - 20, height: self.frame.height - 20)
        
        
        
        
        
        
        self.tickBtn.frame = CGRect(x: self.frame.size.width - 20, y: 0, width: 20, height: 20)
        self.rotateBtn.frame = CGRect(x: 0, y: self.frame.size.height - 20, width: 20, height: 20)
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
