//
//  UtilityObj.m
//  Vidmoji
//
//  Created by iBuildx_Mac_Mini on 9/7/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#import "UtilityObj.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@implementation UtilityObj


+ (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    //NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}



+ (void)loadCameraRollAssetToInstagram:(NSURL*)assetsLibraryURL andMessage:(NSString*)message
{
    NSString *escapedString   = [self urlencodedString:assetsLibraryURL.absoluteString];
    NSString *escapedCaption  = [self urlencodedString:message];
    NSURL *instagramURL       = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@", escapedString, escapedCaption]];
    
    [[UIApplication sharedApplication] openURL:instagramURL];
}

+ (NSString*)urlencodedString :(NSString *)msg
{
    return [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}

@end
