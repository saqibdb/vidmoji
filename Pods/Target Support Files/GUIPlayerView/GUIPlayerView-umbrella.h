#import <UIKit/UIKit.h>

#import "GUIPlayerView.h"
#import "GUISlider.h"
#import "UIView+UpdateAutoLayoutConstraints.h"

FOUNDATION_EXPORT double GUIPlayerViewVersionNumber;
FOUNDATION_EXPORT const unsigned char GUIPlayerViewVersionString[];

